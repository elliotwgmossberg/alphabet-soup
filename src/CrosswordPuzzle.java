public class CrosswordPuzzle {

    private int height;
    private int width;
    private char[][] crossword;

    private int[][] matrixDirections = {{1,0},{1,1},{0,1},{-1,1},{-1,0},{-1,-1},{0,-1},{1,-1}};


    CrosswordPuzzle(int[] puzzleDimension){
        //initialize row X Col sized data array storage
        height = puzzleDimension[0];
        width = puzzleDimension[1];
        crossword = new char[height][width];
    }

    public void enterRowData(int row, char[] letters){

        for(int i = 0; i < letters.length; i++){
            crossword[row][i] = letters[i];
        }
    }

    public String[] searchForWord(char[] word) {
/*      this DFS searches r/c for first letter of word when found begin getDirection,
        getDirection will find the index directions of all second matching letters, or returns null on failed character match.
        if getDirection fails to find any second letter match, it will return to searchforWord and look for a new first letter match and continue.
        if it succeeds in matching any second characters, it will start searchDirForWord in each direction using index direction to find the whole word,
        or return null if there is no full match in the direction.*/


        String startFoundIndex;
        int[] endFoundIndex;

        //row/col search
        for (int row = 0; row < width; row++) {
            for(int col = 0; col < height; col++){

                //find first letter of current word search, then begin DFS
                if(crossword[row][col] == word[0]){
                    startFoundIndex = row + ":" + col; //set starting index for output
                    endFoundIndex = getDirection(new int[]{row,col}, word); //try to find second character
                    col++;

                    //if word was found format and return output string
                    if (endFoundIndex != null){
                        return new String[] {startFoundIndex,endFoundIndex[0] + ":" + endFoundIndex[1]};
                    }

                }
            }
        }
        return null;
    }

    private int[] getDirection(int[] inLocation, char[] searchWord) {

        char searchLetter = searchWord[1];
        int[] endIndex;

        //System.out.println("Searching for valid letters around: " + inLocation[0] + ":" + inLocation[1] );

        //traverse in all directions search for second character matches and return directional index of match
        for (int[] dir : matrixDirections) {

            int row=inLocation[0]+dir[0];
            int col=inLocation[1]+dir[1];

            //Edge case if at edge of bounds
            if(row< 0 || row>= height || col < 0 || col >= width){
                continue;
            }

            //check the location for next letter
            if(crossword[row][col] == searchLetter){
               //System.out.println("second letter match at: " + row + ":" + col );
               endIndex = searchDirForWord(row,col, dir, searchWord);

               if(endIndex != null){
                   return endIndex;
               }
            }
        }
        return null;
    }

    private int[] searchDirForWord( int row, int col, int[] dir, char[] searchWord){

        //increments in the direction provided until the whole word is found and returns end index,
        // hits end of row/col returns null
        int curRow = row;
        int curCol = col;
        int lettercount = 2;

        for(lettercount = 2; lettercount < searchWord.length; lettercount++){
            //edge case check if out-of-bounds
            if(curRow <= 0 || curRow >= height || curCol <= 0 || curCol >= width){
                return null;
            }

            //increment and check if character still matches, break if not
            curRow = curRow + dir[0];
            curCol = curCol + dir[1];


            if(crossword[curRow][curCol] != searchWord[lettercount]){
                return null;
            }
        }

        //check if whole word was found, return end index. return null if not.
        //System.out.println("Match Found");
         return new int[]{curRow,curCol};

    }
}
