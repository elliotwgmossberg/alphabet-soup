import java.io.File;
import java.util.Scanner;

import java.util.logging.FileHandler;

public class Main {
    public static void main(String[] args) {

        //Setup the in/out directory using Files for a FileHandler to read/write with. Check if exists, create if false.
        String dirAsString = "./resources";
        File Directory = new File(dirAsString);
        if(!Directory.exists()) {
            Directory.mkdirs();
        }

        try{

            //Store File's lines to a List, parse for data, execute word search
            for(File FileEntry : Directory.listFiles()) {

                System.out.println("Currently reading file: " + FileEntry.getName());

                //create crossword puzzle and scanner.
                CrosswordPuzzle crosswordPuzzle;
                Scanner scanner = new Scanner(FileEntry);

                //get Data Dimensions, initialize the crossword object
                int[] dimensions = readDataDimensions(scanner.nextLine(), 'x');
                crosswordPuzzle = new CrosswordPuzzle(dimensions);

                //parse crossword rows, and add to crossword puzzle
                for(int i = 0; i < dimensions[0]; i++ ) {
                    crosswordPuzzle.enterRowData(i,readPuzzleLine(scanner.nextLine(), dimensions[1]));
                }

                //parse each word to be searched, and pass to CrosswordPuzzle for search
                while(scanner.hasNextLine()){
                    String rawWord = scanner.nextLine();
                    char[] parsedWord = readSearchWords(rawWord);
                    //System.out.println("Searching for : " + rawWord);
                    String[] wordIndices = crosswordPuzzle.searchForWord(parsedWord);

                    //print result to console
                    if (wordIndices == null){
                        System.out.println("Word Was Not Found");
                    }
                    else{
                        System.out.println( rawWord + " " + wordIndices[0]+ " " + wordIndices[1]);
                    }
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public static int[] readDataDimensions(String inLine, char delim){
        int[] rowCol = {0,0};

        //find delimiter, cut string in two at delim, trim leading/trailing whitespace. assign to row/col format
        for(int i = 0; i < inLine.length(); i++) {
            if(inLine.charAt(i) == delim) {
               rowCol[0] = Integer.parseInt(inLine.substring(0,i).trim());
               rowCol[1] = Integer.parseInt(inLine.substring(i+1,inLine.length()).trim());
               break;
            }
        }
        return rowCol;
    }

    public static char[] readPuzzleLine(String inLine, int rowSize){

        //read character by character, ignore whitespace, add other characters to array, return array.
        char[] foundChars = new char[rowSize];
        int charCount = 0;
        //check each entry against whitespace, if not whitespace add to character array, increment count.
        for(int i = 0; i < inLine.length(); i++){
            if(inLine.charAt(i) != ' '){
                foundChars[charCount] = inLine.charAt(i);
                charCount++;
            }
        }

        return foundChars;
    }

    public static char[] readSearchWords(String inLine){

        String tempLine = inLine.trim();

        int spaceCount = 0;
        char[] word;

        //get num of whitespaces to initialize array of correct size
        for(int i = 0; i < tempLine.length(); i++){
            if(tempLine.charAt(i) == ' ') {
                spaceCount++;
            }
        }
        word = new char[tempLine.length() - spaceCount];
        int wordIndex = 0;
        //read character by character ignoring white-space and add to char array, return array.
        for(int i = 0; i < inLine.length(); i++){
            if(inLine.charAt(i) != ' ') {
                word[wordIndex] = tempLine.charAt(i);
                wordIndex++;
            }
        }
        return word;
    }
}

